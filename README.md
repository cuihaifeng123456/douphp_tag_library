###douphp标签库,by366131726@qq.com

>告别繁琐的$smarty->assign，简简单单的使用标签来搞定，让您有更多的时间去打游戏or把妹！

*   安装说明

>直接将文件复制到根目录即可使用

*   标签使用说明

>标签全部以dou_开头，和您使用{foreach}{/foreach}一样简单，所有标签都可指定一个name属性来实现类似于foreach当中的item效果。

*   导航标签-dou_nav
>使用该标签可以全站调用导航标签，程序会自动判断当前脚本所处模块，并且高亮菜单，支持栏目调用。<br/>
调用方法

>{dou_nav type="top|middle|bottom|mobile|column" pid="上级id" module="当前模块" current_id="当前模块id" current_parent_id="当前上级id"
<br/>
{$nav.nav_name}{$nav.url}....
<br/>{/dou_nav}

*   栏目标签-dou_column
>当使用{dou_nav type="column}的时候，使用此标签可以获取到上级栏目的值。<br/>
{dou_column}{$column.nav_name}{/dou_column}

* url生成标签-dou_url
>用法同$dou->rewrite_url一致，{dou_url module='product_category' value='1'}"  获取id为1的产品分类的链接


* 列表读取标签【支持分页,支持单页】-dou_list
>本标签支持分页，当需要定制化的调用的时候非常有用，比如我想在某个单页调用某个分类下的文章，而且支持分页。<br/>
  {dou_list module='article' cat_id = '1' name=article if_pager=true num=5}{/dou_list}<br/>
  {$dou_pager}使用此标签来显示分页html代码【支持页码显示】<br/>
    
  * 分类读取标签-dou_category
  >用法同get_category方法一致<br/>
  {dou_category module='article_category' parent_id=0 current_id=1}{/dou_category}
  
  * 幻灯读取标签-dou_show
  >用法同{foreach $show_list item=show}方法一致。<br/>
  {dou_show type='pc|mobile'}<br/>{$show.show_img}{$show.show_link}{$show.show_name}<br/>{/dou_show}
    
* 面包屑标签-dou_ur_here【自动获取，无需传参】

>本标签不支持传参，使用该标签系统会根据当前页面的模块来自动生成。<br/>
{dou_ur_here}

*   单页读取标签-dou_page
->该标签可传入单页id直接获取单页的内容。<br/>
{dou_page id='1'}{/if}

*   友情链接读取-dou_link

>用法同{foreach $link item=link}{/foreach}一致。<br/>
{dou_link}{$link.link_name}{$link.link_url}{/dou_link}




