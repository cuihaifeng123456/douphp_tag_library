<?php
/**
 * douphp获取分类标签
 *
 * 参数：
 * module       分类名称 article_category
 * current_id   当前页面的分类id
 * parent_id    上级分类的id，用于读取哪个分类下的二级分类 默认为0
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_category($params, $content, &$smarty, &$repeat)
{

    global $dou;
    require_once 'common.func.php';
    extract($params);
    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'category';
    } else {
        $return = $params['name'];
    }

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }

    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);
    if (@!$smarty->block_data[$dataIndex]) {

        $_MODULE = $dou->dou_module();
        $current_module_arr = get_current_module();
        $current_module = $current_module_arr['module'];
        $current_module_id = $current_module_arr['module_id'];
        $current_module = isset($module) ? $module : $current_module;
        if (strpos($current_module, '_category') === false) {
            $current_module = $current_module . '_category';
            //子页面
            $current_module_id = $GLOBALS['cat_id'] ? $GLOBALS['cat_id'] : 0;
        }
        //当前分类id
        $current_id = isset($current_id) ? $current_id : $current_module_id;
        //上级分类id
        $parent_id = isset($parent_id) ? $parent_id : 0;
        $category = $dou->get_category($current_module, $parent_id, $current_id);
        $smarty->block_data[$dataIndex] = $category;
    }

    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {
        $item['index'] = $key;
        $item['iteration'] = $key + 1;
        if ($key == count($smarty->block_data[$dataIndex]) - 1) {
            $item['last'] = true;
        }
        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;

}



