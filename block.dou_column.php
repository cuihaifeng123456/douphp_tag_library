<?php
/**
 * douphp栏目标签配合dou_nav使用
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_column($params, $content, &$smarty)
{

    global $dou,$firewall;
    require_once 'common.func.php';
    //获取当前页面的
    extract($params);
    $type = 'middle';
    $parent_id = 0;   //上级id
    //当前模块名
    $_MODULE = $dou->dou_module();
    $current_module_arr = get_current_module();
    $current_module =$current_module_arr['module'];
    $current_module_id = $current_module_arr['module_id'];

    $current_module = isset($module) ? $module : $current_module;
    if (in_array($current_module, $_MODULE['column']) && strpos($current_module, '_category') === false) {
        $current_module = $current_module . '_category';
        //子页面
        $current_module_id = $GLOBALS['cat_id']?$GLOBALS['cat_id']:0;
    }

    $current_id = isset($current_id)?$current_id:$current_module_id;

    $current_parent_id = isset($current_parent_id) ? $current_parent_id : 0;
    $sql = "SELECT * FROM " . $dou->table('nav') . " WHERE module='{$current_module}' AND guide = '{$current_id}'";

    $query = $dou->query($sql);

    $current_nav = $dou->fetch_array($query);

    if (!$parent_id) {
        $current_parent_id = $current_nav['parent_id'];
    }


    if ($current_parent_id == 0) {

        $query = $dou->select($dou->table('nav'), '*', '`id` = \'' . $current_nav['id'] . '\'');
    } else {
        $query = $dou->select($dou->table('nav'), '*', '`id` = \'' . $current_parent_id . '\'');
    }
    $top_nav = $dou->fetch_array($query);

    //默认变量名
    if (!isset ($name)) {
        $return = 'column';
    } else {
        $return = $name;
    }

    $smarty->assign($return, $top_nav);

    return $content;
}




