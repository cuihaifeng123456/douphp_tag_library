<?php
/**
 * douphp获取友情链接
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_link($params, $content, &$smarty, &$repeat)
{

    global $dou;
    extract($params);
    require_once 'common.func.php';
    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'link';
    } else {
        $return = $params['name'];
    }

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }

    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);
    if (@!$smarty->block_data[$dataIndex]) {

        $list = get_link_list();
        $smarty->block_data[$dataIndex] = $list;
    }

    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {
        $item['index'] = $key;
        if ($key == count($smarty->block_data[$dataIndex]) - 1) {
            $item['last'] = true;
        }
        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;

}


