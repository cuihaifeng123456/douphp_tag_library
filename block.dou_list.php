<?php
/**
 * douphp获取列表标签【支持分页】
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_list($params, $content, &$smarty, &$repeat)
{

    global $dou;

    require_once 'common.func.php';

    extract($params);

    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'item';
    } else {
        $return = $params['name'];
    }

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }


    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);
    if (@!$smarty->block_data[$dataIndex]) {

        $_MODULE = $dou->dou_module();
        $current_module_arr = get_current_module();
        $current_module = $current_module_arr['module'];
        $current_module_id = $current_module_arr['module_id'];
        //如果是分类页面，自动调用的时候需要去掉_category
        if ($pos = strpos($current_module, '_category')) {
            $current_module = substr($current_module, 0, $pos);
        }else{
            $current_module_id = $GLOBALS['cat_id'] ?$GLOBALS['cat_id'] :0;
        }

        $current_module = isset($module) ? $module : $current_module;

        $cat_id = isset($cat_id) ? $cat_id : $current_module_id;
        if ($current_module == 'page') {
            $where = "  WHERE parent_id = '{$cat_id}'";
        } else {

            $where = $cat_id == 0 ? '' : "  WHERE cat_id IN (" . $cat_id . $dou->dou_child_id($current_module . '_category', $cat_id) . ")";

        }
        $sort = isset($sort) ? $sort . ',' : '';
        $psize = isset($num) ? $num : 10;           //读取数量，默认为10
        $if_pager = isset($if_pager) ? $if_pager : false;       //是否分页
        if ($if_pager) {
            $pindex = max($_REQUEST['page'], 1);
            $limit = " LIMIT " . ($pindex - 1) * $psize . ',' . ($pindex * $psize);
            $sql = "SELECT * FROM " . $dou->table($current_module) . $where;
            $total = mysql_num_rows($dou->query($sql));
            $smarty->assign('dou_pager', pagination($total, $pindex, $psize));
        } else {
            $limit = " LIMIT {$psize}";
        }

        if ($current_module == 'page') {
            $sql = "SELECT * FROM " . $dou->table($current_module) . $where . " ORDER BY id ASC" . $limit;
            $query = $dou->query($sql);
            while ($row = $dou->fetch_array($query)) {
                $row['url'] = $dou->rewrite_url('page', $row['id']);
                $list[] = $row;
            }
        } else {

            $sql = "SELECT * FROM " . $dou->table($current_module) . $where . " ORDER BY " . $sort . "id DESC" . $limit;

            $query = $dou->query($sql);
            while ($row = $dou->fetch_array($query)) {
                $item['id'] = $row['id'];
                if ($row['title']) $item['title'] = $row['title'];
                if ($row['name']) $item['name'] = $row['name'];
                if (!empty($row['price'])) $item['price'] = $row['price'] > 0 ? $dou->price_format($row['price']) : $GLOBALS['_LANG']['price_discuss'];
                if ($row['click']) $item['click'] = $row['click'];
                $item['add_time'] = date("Y-m-d", $row['add_time']);
                $item['add_time_short'] = date("m-d", $row['add_time']);
                $item['description'] = $row['description'] ? $row['description'] : $dou->dou_substr($row['content'], 220);
                $item['image'] = $row['image'] ? ROOT_URL . $row['image'] : '';
                $item['add_time_int'] = $row['add_time'];

                $item['url'] = $dou->rewrite_url($current_module, $row['id']);
                $item['video'] = $row['video'];
                $item['click'] = $row['click'];

                $list[] = $item;
            }
        }

        //加入chunk支持

        if (isset($chunk)) {
            $list = array_chunk($list, $chunk);
        }
        $smarty->block_data[$dataIndex] = $list;

    }

    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {

//        @todo 分块下暂不支持，考虑css实现
        if (!isset($chunk)) {
            $item['index'] = $key;
            $item['iteration'] = $key + 1;
            if ($key == count($smarty->block_data[$dataIndex]) - 1) {
                $item['last'] = true;
            }
        }

        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;

}









