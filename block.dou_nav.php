<?php
/**
 * douphp导航标签【支持栏目调用】
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_nav($params, $content, &$smarty, &$repeat)
{

    global $dou, $firewall;
    //获取当前页面的
    extract($params);
    require_once 'common.func.php';
    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'nav';
    } else {
        $return = $params['name'];
    }

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }

    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);

    if (@!$smarty->block_data[$dataIndex]) {


        $type = isset($type) ? $type : 'middle';
        $parent_id = isset($pid) ? $pid : 0;   //上级id
        //当前模块名
        $_MODULE = $dou->dou_module();
        $current_module_arr = get_current_module();
        $current_module =$current_module_arr['module'];
        $current_module_id = $current_module_arr['module_id'];

        $current_module = isset($module) ? $module : $current_module;
        if (in_array($current_module, $_MODULE['column']) && strpos($current_module, '_category') === false) {
            $current_module = $current_module . '_category';
            //子页面
            $current_module_id = $GLOBALS['cat_id'] ? $GLOBALS['cat_id'] : 0;
        }


        $current_id = isset($current_id) ? $current_id : $current_module_id;


        $current_parent_id = isset($current_parent_id) ? $current_parent_id : 0;
        $sql = "SELECT * FROM " . $dou->table('nav') . " WHERE module='{$current_module}' AND guide = '{$current_id}'";

        $query = $dou->query($sql);

        $current_nav = $dou->fetch_array($query);


        if (!$parent_id) {
            $current_parent_id = $current_nav['parent_id'];
        }


        //重写系统方法，支持栏目调用[暂时只支持一级栏目]
        if ($type == 'middle' || $type == 'top' || $type == 'bottom' || $type == 'mobile') {
            $nav_list = get_dou_nav($type, $parent_id, $current_module, $current_id, $current_parent_id);
        } else {


            if ($current_parent_id == 0) {
                $nav_list = get_dou_nav('middle', $current_nav['id'], $current_module, $current_id, $current_parent_id);
            } else {
                $nav_list =get_dou_nav('middle', $current_parent_id, $current_module, $current_id, $current_parent_id);
            }

        }

        $smarty->block_data[$dataIndex] = $nav_list;
    }


    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {
        $item['index'] = $key;
        if($key == count($smarty->block_data[$dataIndex]) -1){
            $item['last'] = true;
        }
        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;
}


