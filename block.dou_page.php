<?php
/**
 * douphp获取单页标签

 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_page($params, $content, &$smarty, &$repeat)
{

    global $dou;
    require_once 'common.func.php';
    extract($params);
    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'page';
    } else {
        $return = $params['name'];
    }

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }

    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);

    if (@!$smarty->block_data[$dataIndex]) {
        $current_module_arr = get_current_module();
        $current_module =$current_module_arr['module'];
        $current_module_id = $current_module_arr['module_id'];
        $id = isset($id)?$id:$current_module_id;
        if($current_module != 'page' && !$id)
        {
            return '当您不传入id的时候，只能在单页内使用！';
        }



        $query = $dou->select($dou->table('page'), '*', '`id` = \'' . $id . '\'');
        $page = $dou->fetch_array($query);

        if ($page) {
            $page['url'] = $dou->rewrite_url('page', $page['id']);
        }
        $smarty->block_data[$dataIndex][0] = $page;
    }


    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {
        $item['index'] = $key;
        if ($key == count($smarty->block_data[$dataIndex]) - 1) {
            $item['last'] = true;
        }
        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;

}



