<?php
/**
 * douphp获取幻灯标签
 *
 * 参数：
 * module       分类名称 article_category
 * current_id   当前页面的分类id
 * parent_id    上级分类的id，用于读取哪个分类下的二级分类 默认为0
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $content
 * @param $smarty
 * @return mixed
 */
function smarty_block_dou_show($params, $content, &$smarty, &$repeat)
{

    global $dou;
    require_once 'common.func.php';
    extract($params);
    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'show';
    } else {
        $return = $params['name'];
    }

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }

    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);
    if (@!$smarty->block_data[$dataIndex]) {
        $type = isset($type) ? $type :'pc';
        $show_list = $dou->get_show_list($type);
        $smarty->block_data[$dataIndex] = $show_list;
    }

    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {
        $item['index'] = $key;
        if($key == count($smarty->block_data[$dataIndex]) -1){
            $item['last'] = true;
        }
        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;

}



