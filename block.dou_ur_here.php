<?php
/**
 * douphp 面包屑生成标签
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_block_dou_ur_here($params, $content, &$smarty, &$repeat)
{
    global $dou;

    //注册一个区块
    if (!isset($smarty->block_data)) {
        $smarty->block_data = array();
    }

    $dataIndex = md5(__FUNCTION__ . md5(serialize($params)));
    $dataIndex = substr($dataIndex, 0, 16);

    //默认变量名
    if (!isset ($params['name'])) {
        $return = 'item';
    } else {
        $return = $params['name'];
    }


    if (@!$smarty->block_data[$dataIndex]) {
        $_MODULE = $dou->dou_module();
        $current_module_arr = get_current_module();
        $current_module = $current_module_arr['module'];
        $current_module_id = $current_module_arr['module_id'];

        if (in_array($current_module, $_MODULE['column']) && strpos($current_module, '_category') === false) {
            $current_module = $current_module . '_category';
            //子页面
            $current_module_id = $GLOBALS['cat_id'] ? $GLOBALS['cat_id'] : 0;
        }
        $sql = "SELECT * FROM " . $dou->table('nav') . " WHERE module='{$current_module}' AND guide = '{$current_module_id}'";

        $query = $dou->query($sql);
        $current_nav = $dou->fetch_array($query);

        $ur_here = array();
        while (true) {
            $ur_here[] = array('name' => $current_nav['nav_name'], 'url' => $dou->rewrite_url($current_nav['module'], $current_nav['guide']));
            if ($current_nav['parent_id'] != 0) {
                $sql = "SELECT * FROM " . $dou->table('nav') . " WHERE id='{$current_nav[parent_id]}'";
                $query = $dou->query($sql);
                $current_nav = $dou->fetch_array($query);
            } else {
                break;
            }

        }
        $ur_here = array_reverse($ur_here);
        $smarty->block_data[$dataIndex] = $ur_here;
    }




    if (!$smarty->block_data[$dataIndex]) {
        $repeat = false;
        return '';
    }

    if (list ($key, $item) = each($smarty->block_data[$dataIndex])) {
        $item['index'] = $key;
        if($key == count($smarty->block_data[$dataIndex]) -1){
            $item['last'] = true;
        }
        $smarty->assign($return, $item);
        $repeat = true;
    }
    //到达末尾
    if (!$item) {
        $repeat = false;
        reset($smarty->block_data[$dataIndex]);
    }
    return $content;

}


?>
