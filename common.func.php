<?php
/**
 * 标签通用函数库
 */


/**
 * 分页函数
 * @param $total
 * @param $pageIndex
 * @param int $pageSize
 * @param string $url
 * @param array $context
 * @return string
 */
function pagination($total, $pageIndex, $pageSize = 15, $url = '', $context = array('before' => 5, 'after' => 4, 'ajaxcallback' => '', 'callbackfuncname' => ''))
{

    if ($GLOBALS['_CFG']['rewrite']) {
        $script_name = current_url();
        unset($_GET['route']);
        $script_name = preg_replace("/\?page=\d/","",$script_name);
    } else {
        $script_name = scriptname();
    }


    $pdata = array(
        'tcount' => 0,
        'tpage' => 0,
        'cindex' => 0,
        'findex' => 0,
        'pindex' => 0,
        'nindex' => 0,
        'lindex' => 0,
        'options' => ''
    );
    if ($context['ajaxcallback']) {
        $context['isajax'] = true;
    }

    if ($context['callbackfuncname']) {
        $callbackfunc = $context['callbackfuncname'];
    }

    $pdata['tcount'] = $total;
    $pdata['tpage'] = (empty($pageSize) || $pageSize < 0) ? 1 : ceil($total / $pageSize);
    if ($pdata['tpage'] <= 1) {
        return '';
    }
    $cindex = $pageIndex;
    $cindex = min($cindex, $pdata['tpage']);
    $cindex = max($cindex, 1);
    $pdata['cindex'] = $cindex;
    $pdata['findex'] = 1;
    $pdata['pindex'] = $cindex > 1 ? $cindex - 1 : 1;
    $pdata['nindex'] = $cindex < $pdata['tpage'] ? $cindex + 1 : $pdata['tpage'];
    $pdata['lindex'] = $pdata['tpage'];

    if ($context['isajax']) {
        if (empty($url)) {
            $url = $script_name . '?' . http_build_query($_GET);
        }
        $pdata['faa'] = 'href="javascript:;" page="' . $pdata['findex'] . '" ' . ($callbackfunc ? 'onclick="' . $callbackfunc . '(\'' . $url . '\', \'' . $pdata['findex'] . '\', this);return false;"' : '');
        $pdata['paa'] = 'href="javascript:;" page="' . $pdata['pindex'] . '" ' . ($callbackfunc ? 'onclick="' . $callbackfunc . '(\'' . $url . '\', \'' . $pdata['pindex'] . '\', this);return false;"' : '');
        $pdata['naa'] = 'href="javascript:;" page="' . $pdata['nindex'] . '" ' . ($callbackfunc ? 'onclick="' . $callbackfunc . '(\'' . $url . '\', \'' . $pdata['nindex'] . '\', this);return false;"' : '');
        $pdata['laa'] = 'href="javascript:;" page="' . $pdata['lindex'] . '" ' . ($callbackfunc ? 'onclick="' . $callbackfunc . '(\'' . $url . '\', \'' . $pdata['lindex'] . '\', this);return false;"' : '');
    } else {
        if ($url) {
            $pdata['faa'] = 'href="?' . str_replace('*', $pdata['findex'], $url) . '"';
            $pdata['paa'] = 'href="?' . str_replace('*', $pdata['pindex'], $url) . '"';
            $pdata['naa'] = 'href="?' . str_replace('*', $pdata['nindex'], $url) . '"';
            $pdata['laa'] = 'href="?' . str_replace('*', $pdata['lindex'], $url) . '"';
        } else {
            $_GET['page'] = $pdata['findex'];


            $pdata['faa'] = 'href="' . $script_name . '?' . http_build_query($_GET) . '"';
            $_GET['page'] = $pdata['pindex'];


            $pdata['paa'] = 'href="' . $script_name . '?' . http_build_query($_GET) . '"';
            $_GET['page'] = $pdata['nindex'];

            $pdata['naa'] = 'href="' . $script_name . '?' . http_build_query($_GET) . '"';
            $_GET['page'] = $pdata['lindex'];
            $pdata['laa'] = 'href="' . $script_name . '?' . http_build_query($_GET) . '"';
        }
    }

    $html = '<div style="text-align: center"><ul class="pagination pagination-centered">';
    if ($pdata['cindex'] > 1) {
        $html .= "<li><a {$pdata['faa']} class=\"pager-nav\">首页</a></li>";
        $html .= "<li><a {$pdata['paa']} class=\"pager-nav\">&laquo;上一页</a></li>";
    }
    if (!$context['before'] && $context['before'] != 0) {
        $context['before'] = 5;
    }
    if (!$context['after'] && $context['after'] != 0) {
        $context['after'] = 4;
    }

    if ($context['after'] != 0 && $context['before'] != 0) {
        $range = array();
        $range['start'] = max(1, $pdata['cindex'] - $context['before']);
        $range['end'] = min($pdata['tpage'], $pdata['cindex'] + $context['after']);
        if ($range['end'] - $range['start'] < $context['before'] + $context['after']) {
            $range['end'] = min($pdata['tpage'], $range['start'] + $context['before'] + $context['after']);
            $range['start'] = max(1, $range['end'] - $context['before'] - $context['after']);
        }
        for ($i = $range['start']; $i <= $range['end']; $i++) {
            if ($context['isajax']) {
                $aa = 'href="javascript:;" page="' . $i . '" ' . ($callbackfunc ? 'onclick="' . $callbackfunc . '(\'' . $url . '\', \'' . $i . '\', this);return false;"' : '');
            } else {
                if ($url) {
                    $aa = 'href="?' . str_replace('*', $i, $url) . '"';
                } else {
                    $_GET['page'] = $i;
                    $aa = 'href="?' . http_build_query($_GET) . '"';
                }
            }
            $html .= ($i == $pdata['cindex'] ? '<li class="active"><a href="javascript:;">' . $i . '</a></li>' : "<li><a {$aa}>" . $i . '</a></li>');
        }
    }

    if ($pdata['cindex'] < $pdata['tpage']) {
        $html .= "<li><a {$pdata['naa']} class=\"pager-nav\">下一页&raquo;</a></li>";
        $html .= "<li><a {$pdata['laa']} class=\"pager-nav\">尾页</a></li>";
    }
    $html .= '</ul></div>';
    return $html;
}

function current_url()
{
    $page_url = "http";
    if ($_SERVER['HTTPS'] == 'on') {
        $page_url .= 's';
    }
    $page_url .= "://";
    if ($_SERVER['SERVER_PORT'] != 80) {
        $page_url .= $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
    } else {
        $page_url .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    }
    return $page_url;
}


/**
 * 工具函数获取当前的脚本名称
 * @return mixed|string
 */
function scriptname()
{

    $scriptName = basename($_SERVER['SCRIPT_FILENAME']);
    if (basename($_SERVER['SCRIPT_NAME']) === $scriptName) {
        $scriptName = $_SERVER['SCRIPT_NAME'];
    } else {
        if (basename($_SERVER['PHP_SELF']) === $scriptName) {
            $scriptName = $_SERVER['PHP_SELF'];
        } else {
            if (isset($_SERVER['ORIG_SCRIPT_NAME']) && basename($_SERVER['ORIG_SCRIPT_NAME']) === $scriptName) {
                $scriptName = $_SERVER['ORIG_SCRIPT_NAME'];
            } else {
                if (($pos = strpos($_SERVER['PHP_SELF'], '/' . $scriptName)) !== false) {
                    $scriptName = substr($_SERVER['SCRIPT_NAME'], 0, $pos) . '/' . $scriptName;
                } else {
                    if (isset($_SERVER['DOCUMENT_ROOT']) && strpos($_SERVER['SCRIPT_FILENAME'], $_SERVER['DOCUMENT_ROOT']) === 0) {
                        $scriptName = str_replace('\\', '/', str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']));
                    } else {
                        $scriptName = 'unknown';
                    }
                }
            }
        }
    }
    return $scriptName;
}

/**
 * 获取当前栏目的模块名和栏目id
 * @return array
 */
function get_current_module()
{
    global $firewall;
    if ($GLOBALS['_CFG']['rewrite']) {
        //为静态下的模块名
        $current_module = $GLOBALS['mark']['module'];
        $current_module_id = $firewall->get_legal_id($current_module, $_REQUEST['id'], $_REQUEST['unique_id']);
    } else {
        $current_module = basename(scriptname(), '.php');
        $current_module_id = $_REQUEST['id'];
    }
    if($current_module_id<=0 || !$current_module_id){
        $current_module_id = 0;
    }
    return array('module'=>$current_module,'module_id'=>$current_module_id);
}



/*
 * 实现系统函数
 */
function get_dou_nav($type = 'middle', $parent_id = 0, $current_module = '', $current_id = '', $current_parent_id = '') {

    global $dou;

    $nav = array ();
    $data = $dou->fetch_array_all($dou->table('nav'), 'sort ASC');
    foreach ((array) $data as $value) {
        // 根据$parent_id和$type筛选父级导航
        if ($value['parent_id'] == $parent_id && $value['type'] == $type) {
            // 如果是自定义链接则$value['guide']值链接地址，如果是内部导航则值是栏目ID
            if ($value['module'] == 'nav') {
                if (strpos($value['guide'], 'http://') === 0 || strpos($value['guide'], 'https://') === 0) {
                    $value['url'] = $value['guide'];
                    // 自定义导航如果包含http则在新窗口打开
                    $value['target'] = true;
                } else {
                    $value['url'] = ROOT_URL . $value['guide'];
                    // 系统会比对自定义链接是否包含在当前URL里，如果包含则高亮菜单，如果不需要此功能，请注释掉下面那行代码
                    $value['cur'] = strpos($_SERVER['REQUEST_URI'], $value['guide']);
                }
            } else {
                $value['url'] = $dou->rewrite_url($value['module'], $value['guide']);
                $value['cur'] = $dou->dou_current($value['module'], $value['guide'], $current_module, $current_id, $current_parent_id);
                if(!$value['cur']){
                    $value['cur'] = $value['id'] == $current_parent_id ? true :false;
                }

            }

            $value['image'] = ROOT_URL.$value['image'];
            foreach ($data as $child) {
                // 筛选下级导航
                if ($child['parent_id'] == $value['id']) {
                    $value['child'] = $dou->get_nav($type, $value['id']);
                    break;
                }
            }
            $nav[] = $value;
        }
    }

    return $nav;
}