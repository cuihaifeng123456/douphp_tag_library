<?php
/**
 * 2018年6月7日更新
 * douphp 缩略图生成标签
 * 作者：366131726@qq.com wechat:c8517062
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_dou_thumb($params, &$smarty)
{
    global $dou;
    extract($params);

    if(!file_exists(ROOT_PATH.'thumb.php')){
        return 'error:缺少缩略图生成文件';
    }

    $data['src'] = isset($src)?$src:'';
    if(!$data['src']){
        return 'error:请传入图片路径';
    }
    $data['src'] = thumb_encrypt($data['src'],"E");



    $data['w']= isset($w)?$w:400;
    $data['h'] = isset($h)?$h:400;
    $data['cut'] = isset($cut)?true:false;  //是否裁剪图片，true）裁掉超过比例的部分；false）不裁剪图片，图片缩放显示，增加白色背景
    $data['cut_place'] = isset($cut_place)?$cut_place:5;    //裁剪保留位置 1:左上, 2：中上， 3右上, 4：左中， 5：中中， 6：右中，7：左下， 8：中下，9右下
    $data['water'] = isset($water)?true:false;
    $data['water_pos'] = isset($water_pos)?$water_pos:9;    //水印放置位置 1:左上, 2：中上， 3右上, 4：左中， 5：中中， 6：右中，7：左下， 8：中下，9右下
    $data['remote'] = isset($remote)?true:false;            //新增外部图片调用
    $query_string = http_build_query($data);
    $thumb_url = ROOT_URL.'thumb.php?'.$query_string;
    return $thumb_url;

}


if(!method_exists('thumb_encrypt')){
    /**
     * 加解密函数
     * @param $string
     * @param $operation
     * @param string $key
     * @return bool|mixed|string
     */
    function thumb_encrypt($string,$operation,$key=DOU_ID){
        $key=md5($key);
        $key_length=strlen($key);
        $string=$operation=='D'?base64_decode($string):substr(md5($string.$key),0,8).$string;
        $string_length=strlen($string);
        $rndkey=$box=array();
        $result='';
        for($i=0;$i<=255;$i++){
            $rndkey[$i]=ord($key[$i%$key_length]);
            $box[$i]=$i;
        }
        for($j=$i=0;$i<256;$i++){
            $j=($j+$box[$i]+$rndkey[$i])%256;
            $tmp=$box[$i];
            $box[$i]=$box[$j];
            $box[$j]=$tmp;
        }
        for($a=$j=$i=0;$i<$string_length;$i++){
            $a=($a+1)%256;
            $j=($j+$box[$a])%256;
            $tmp=$box[$a];
            $box[$a]=$box[$j];
            $box[$j]=$tmp;
            $result.=chr(ord($string[$i])^($box[($box[$a]+$box[$j])%256]));
        }
        if($operation=='D'){
            if(substr($result,0,8)==substr(md5(substr($result,8).$key),0,8)){
                return substr($result,8);
            }else{
                return'';
            }
        }else{
            return str_replace('=','',base64_encode($result));
        }
    }
}


?>
